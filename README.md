# README #

`mvn clean package spring-boot:repackage -DskipTests && java -jar -Dspring.profiles.active=test ./target/das-boot-1.0-SNAPSHOT.jar`

Samples: https://github.com/spring-projects/spring-boot/tree/master/spring-boot-samples

Java Cryptography:
`sudo add-apt-repository ppa:webupd8team/java
sudo apt update
sudo apt install oracle-java7-unlimited-jce-policy`

### Introduce angular4 client ###

https://github.com/akveo/ngx-admin
```$xslt
steps:
Visual Studio Code - Angular support
Install Node.js: node -v && npm -v
Install AngularCLI: https://github.com/angular/angular-cli
    npm install -g @angular/cli
    ng new PROJECT-NAME
    cd PROJECT-NAME

    npm install //on current project

    ng serve
    ng serve --host 0.0.0.0 --port 4201
    
    ng build --env=prod && clean package spring-boot:repackage -DskipTest=false
    and run the application....
```
for Vaadin framework use SpringBootVaadin tag or Vaadin branch

For large json objects just use:
`http://www.jsonschema2pojo.org/`

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* @makowey