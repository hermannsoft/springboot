package com.boot;

import com.boot.controller.UserController;
import com.boot.model.User;
import com.boot.repository.UserRepository;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserControllerTest {

    @InjectMocks
    private UserController userController;

    @Mock
    private UserRepository userRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testUserGet() {
        User user = new User();
        user.setId(1L);
        when(userRepository.findOne(1L)).thenReturn(user);

        User userTest = userController.getById(1L);

        verify(userRepository).findOne(1L);

        assertEquals(1L, userTest.getId().longValue());

        assertThat(userTest.getId(), Matchers.is(1L));
    }
}
