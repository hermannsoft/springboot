package com.boot;

import com.boot.controller.HomeController;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DasBootTest {

    @Test
    public void testHome() {
        HomeController homeController = new HomeController();
        String result = homeController.home();

        assertEquals(result, "Das Boot, reporting for duty!");
    }
}
