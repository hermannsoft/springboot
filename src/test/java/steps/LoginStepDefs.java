package steps;

import com.boot.model.User;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

import java.util.List;

public class LoginStepDefs {
    @cucumber.api.java.en.Given("^I navigate to the login page$")
    public void iNavigateToTheLoginPage() throws Throwable {

        System.out.println("iNavigateToTheLoginPage");
    }

    @And("^I click login button$")
    public void iClickLoginButton() throws Throwable {

        System.out.printf("iClickLoginButton");
    }

    @Then("^I should see the userform page$")
    public void iShouldSeeTheUserformPage() throws Throwable {

        System.out.println("This is a @then step - iShouldSeeTheUserformPage");
    }

    @And("^I enter the username as \"([^\"]*)\" and password as \"([^\"]*)\"$")
    public void iEnterTheUsernameAsAndPasswordAs(String username, String password) throws Throwable {

        System.out.printf("\niEnterTheUsernameAsAdminAndPasswordAsAdmin[%s, %s]\n\n", username, password);
    }

    @And("^I enter the following for Login$")
    public void iEnterTheFollowingForLogin(DataTable dataTable) throws Throwable {

        List<List<String>> data = dataTable.raw();

        List<User> users = dataTable.asList(User.class);

        for (User user : users) {
            System.out.printf("\n [%d]The user %s has %d years\n\n", user.getId(), user.getName(), user.getAge());
        }

        System.out.println("I enter the following for Login: " + data);
    }
}
