Feature: LoginFeature
  This feature deals with the login functionality og the application

  Scenario: Login with correct username and password
    Given I navigate to the login page
    And I enter the username as "admin" and password as "test"
    And I enter the following for Login
      | id | name  | age |
      | 1  | admin | 34  |
      | 2  | root  | 26  |
    And I click login button
    Then I should see the userform page
