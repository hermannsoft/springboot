package com.boot.ui;

import com.boot.model.User;
import com.boot.model.storage.rds.json.DigiStorageAPI;
import com.boot.model.storage.rds.json.UserBookmarks;
import com.boot.model.storage.rds.json.UserConnections;
import com.boot.model.storage.rds.json.model.devices.Devices;
import com.boot.model.storage.rds.json.model.files.Blob;
import com.boot.model.storage.rds.json.model.files.Files;
import com.boot.model.storage.rds.json.model.files.Type;
import com.boot.model.storage.rds.json.model.mounts.Mounts;
import com.boot.repository.UserRepository;
import com.boot.services.DigiStorage;
import com.boot.ui.controllers.UserEditor;
import com.vaadin.annotations.JavaScript;
import com.vaadin.annotations.Theme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.server.Resource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.boot.model.storage.rds.json.DigiStorageAPI.API_BOOKMARKS;
import static com.boot.services.DigiStorage.MOUNT_ID;
import static com.vaadin.ui.Notification.Type.TRAY_NOTIFICATION;
import static com.vaadin.ui.Notification.Type.WARNING_MESSAGE;

@SpringUI
@Theme("valo")
@JavaScript({"vaadin://plugins/jquery-3.2.1.min.js"})
public class VaadinUI extends UI {

    public static final String SOMETHING_GOES_WRONG_STATE = "Something goes wrong - state\n\n ";
    public static final String NO_API_SELECTED = "NO API SELECTED";
    public static final String S_CALL_PERFORMED = "%s call performed";
    public static final String DEFAULT_TOKEN = "eda20d3f-f20b-4963-8e17-ee9a9d95dc59";
    public static final String EMAIL_USER = "makowey@gmail.com";
    public static final String PASSWORD_USER = "bd5v edif jb8q swxm";
    public static final String MEDIA_PLAYER_CAPTION = "DigiStorage Media Player";
    public static final String S_S_S_MEDIA_PLAYER_INDEX_HTML = "%s://%s%s/mediaPlayer/index.html";
    public static final String HTTPS = "https";

    private final Grid<User> grid;
    private final TextField filter;
    private final UserRepository repo;
    private final UserEditor editor;
    private final Button addNewBtn;

    // DigiStorage related
    private final TextField apiPath;
    private final Button mediaPlayerButton;
    private final Button performRequestButton;
    private final TextField tokenValue;
    private final TextArea content;
    private final ComboBox apiSelector;
    private final TextField mediaLink;
    private final ComboBox filesAPIType;
    private final TextField filesAPIMountId;
    private final TextField filesAPIPath;

    private final FormLayout mediaPlayerLayout = new FormLayout();
    private HorizontalLayout filesLayoutAPI;
    private DigiStorage digiStorage;

    @Autowired
    public VaadinUI(UserRepository repo, UserEditor editor) {
        this.repo = repo;
        this.editor = editor;
        this.grid = new Grid<>(User.class);
        this.filter = new TextField();
        this.addNewBtn = new Button("New user", VaadinIcons.PLUS);

        this.content = new TextArea();
        this.apiPath = new TextField();
        this.performRequestButton = new Button("REQUEST");
        this.mediaPlayerButton = new Button("Start player");
        this.tokenValue = new TextField();
        this.apiSelector = new ComboBox();

        this.mediaLink = new TextField();
        this.mediaLink.setPlaceholder("Add a media url");

        this.filesAPIType = new ComboBox();
        this.filesAPIType.setEmptySelectionAllowed(false);
        this.filesAPIType.setTextInputAllowed(false);
        this.filesAPIMountId = new TextField();
        this.filesAPIMountId.setPlaceholder("mound id");
        this.filesAPIPath = new TextField();
        this.filesAPIPath.setPlaceholder("filename");

    }

    @Override
    protected void init(VaadinRequest request) {

        // build layout
        HorizontalLayout actions = new HorizontalLayout(filter, addNewBtn);
        VerticalLayout userLayout = new VerticalLayout(actions, grid, editor);

        filesLayoutAPI = new HorizontalLayout(filesAPIType, filesAPIMountId, filesAPIPath);

        HorizontalLayout apiActions = new HorizontalLayout();
        apiActions.addComponents(apiSelector, performRequestButton);
        apiActions.setComponentAlignment(performRequestButton, Alignment.BOTTOM_RIGHT);

        FormLayout apiForm = new FormLayout(apiActions, filesLayoutAPI, apiPath, tokenValue,
                content, mediaLink, mediaPlayerButton, mediaPlayerLayout);
        mediaPlayerLayout.setSizeFull();

        HorizontalLayout apiLayout = new HorizontalLayout(userLayout, apiForm);

        URI location = Page.getCurrent().getLocation();
        String mediaPlayerURL = String.format(S_S_S_MEDIA_PLAYER_INDEX_HTML,
                location.getScheme(),
                location.getHost(),
                !location.getScheme().contains(HTTPS) ? ":" + location.getPort() : ""
        );

        new Notification(mediaPlayerURL, Notification.Type.TRAY_NOTIFICATION).show(Page.getCurrent());
        BrowserFrame mediaPlayer = new BrowserFrame(MEDIA_PLAYER_CAPTION, new ExternalResource(mediaPlayerURL));

        mediaPlayer.setSizeFull();
        mediaPlayer.setHeight("100%");
        HorizontalLayout mediaPlayerLayout = new HorizontalLayout(mediaPlayer);
        mediaPlayerLayout.setSizeFull();
        mediaPlayerLayout.setMargin(false);
        mediaPlayerLayout.setSpacing(false);

        Page.Styles styles = Page.getCurrent().getStyles();
        styles.add(".v-slot .v-has-caption .v-caption { display: none; }");

        VerticalLayout mainLayout = new VerticalLayout(mediaPlayerLayout, apiLayout);
        setContent(mainLayout);
        mainLayout.setMargin(false);
        mainLayout.setSpacing(false);

        // List of APIs
        List<DigiStorageAPI> apis = new ArrayList<>(Arrays.asList(DigiStorageAPI.values()));
        apis.remove(DigiStorageAPI.API_TOKEN);

        apiSelector.setValue(API_BOOKMARKS);
        apiSelector.setItems(apis);
        apiSelector.setEmptySelectionAllowed(false);
        apiSelector.setTextInputAllowed(false);
        apiSelector.addValueChangeListener(e -> extendFilesAPIFilter());

        List<Type> filesAPI = new ArrayList<>(Arrays.asList(Type.values()));
        filesAPIType.setItems(filesAPI);

        String widthApiFields = "600";
        apiPath.setWidth(widthApiFields);
        content.setWidth(widthApiFields);
        tokenValue.setWidth(widthApiFields);
        mediaLink.setWidth(widthApiFields);
        content.setRows(9);

        apiSelector.setWidth("300");

        grid.setHeight(300, Unit.PIXELS);
        grid.setColumns("id", "name", "email", "age", "token");

        filter.setPlaceholder("Filter by Name");

        // Hook logic to components

        // Replace listing with filtered content when user changes filter
        filter.setValueChangeMode(ValueChangeMode.LAZY);
        filter.addValueChangeListener(e -> listUsers(e.getValue()));

        // Connect selected User to editor or hide if none is selected
        grid.asSingleSelect().addValueChangeListener(e -> handleSelectedGridItem(e.getValue()));

        // Instantiate and edit new User the new button is clicked
        addNewBtn.addClickListener(e -> editor.editUser(new User("", 0, tokenValue.getValue())));

        // Listen changes made by the editor, refresh data from backend
        editor.setChangeHandler(() -> {
            editor.setVisible(false);
            listUsers(filter.getValue());
        });

        // Initialize listing
        listUsers(null);

        performRequestButton.addClickListener(e -> callDigiAPI());
        mediaPlayerButton.addClickListener(e -> startMusic());

        initDigiAPI();
    }

    private void extendFilesAPIFilter() {
        if (apiSelector.getSelectedItem().isPresent()) {
            filesLayoutAPI.setVisible(apiSelector.getSelectedItem().get().equals(DigiStorageAPI.API_FILES));
        }
    }

    private void startMusic() {

        if (!mediaLink.isEmpty()) {

            if (mediaLink.getValue().endsWith(".mp3")) {
                audioPlayer(mediaPlayerLayout, mediaLink.getValue());
            }

            if (mediaLink.getValue().endsWith(".mp4")) {
                videoPlayer(mediaPlayerLayout, mediaLink.getValue(), mediaLink.getValue().replace("mp4", "ogv"));
            }

            if (mediaLink.getValue().contains(".jpg")) {
                imageLoader(mediaPlayerLayout, mediaLink.getValue());
            }
        } else {

            Notification notification = new Notification("No valid url", WARNING_MESSAGE);
            notification.setPosition(Position.TOP_RIGHT);
            notification.show(this.getPage());
        }
    }

    private void initDigiAPI() {

        String defaultToken = DEFAULT_TOKEN;
        tokenValue.setValue(defaultToken);
        digiStorage = new DigiStorage(defaultToken);

        apiSelector.setSelectedItem(DigiStorageAPI.API_BOOKMARKS);
        apiPath.setValue(DigiStorageAPI.API_BOOKMARKS.getAPI());
        apiPath.setEnabled(false);

        content.setPlaceholder("Press <REQUEST> button in order to get endpoint results.\n\nFor media just add an valid url with audio, video or image and press <Start player>");

        if (tokenValue.isEmpty()) {
            digiStorage = new DigiStorage();
            digiStorage.login(EMAIL_USER, PASSWORD_USER);

            tokenValue.setValue(digiStorage.getAccessToken());
            tokenValue.setEnabled(false);

            content.setValue(digiStorage.getBookmarksFromAPI().toString());
        }

    }

    private void handleSelectedGridItem(User user) {

        if (user != null) {
            tokenValue.setValue(user.getToken());
            digiStorage = new DigiStorage(user.getToken());
            editor.editUser(user);
        }
    }

    private void callDigiAPI() {

        if (apiSelector.getSelectedItem().isPresent()) {

            DigiStorageAPI currentDigiStorageAPI = (DigiStorageAPI) apiSelector.getSelectedItem().get();
            apiPath.setValue(currentDigiStorageAPI.getAPI());

            content.setCaptionAsHtml(true);
            switch (currentDigiStorageAPI) {

                case API_BOOKMARKS:
                    UserBookmarks userBookmarks = digiStorage.getBookmarksFromAPI();

                    content.setValue(userBookmarks.getStateObject().isEmpty() ?
                            userBookmarks.toString() :
                            SOMETHING_GOES_WRONG_STATE + userBookmarks.getStateObject());
                    break;

                case API_CONNECTIONS:
                    UserConnections userConnections = digiStorage.getConnectionsFromAPI();

                    content.setValue(userConnections.getStateObject().isEmpty() ?
                            userConnections.toString() :
                            SOMETHING_GOES_WRONG_STATE + userConnections.getStateObject());
                    break;

                case API_MOUNTS:
                    Mounts mounts = digiStorage.getMountsFromAPI();

                    content.setValue(mounts.getStateObject().isEmpty() ?
                            mounts.toString() :
                            SOMETHING_GOES_WRONG_STATE + mounts.getStateObject());
                    break;

                case API_DEVICES:
                    Devices devices = digiStorage.getDevicesFromAPI();

                    content.setValue(devices.getStateObject().isEmpty() ?
                            devices.toString() :
                            SOMETHING_GOES_WRONG_STATE + devices.getStateObject());
                    break;

                case API_FILES:
                    if (filesAPIType.getSelectedItem().isPresent()) {
                        Blob blob = digiStorage.getBlob(filesAPIMountId.getValue(), filesAPIPath.getValue());

                        content.setValue(blob.getStateObject().isEmpty() ?
                                blob.toString() :
                                SOMETHING_GOES_WRONG_STATE + blob.getStateObject());
                    } else {
                        Files files = digiStorage.getFilesFromAPI(Type.LIST, MOUNT_ID, "");

                        content.setValue(files.getStateObject().isEmpty() ?
                                files.toString() :
                                SOMETHING_GOES_WRONG_STATE + files.getStateObject());
                    }
                    break;

                default:
                    content.setValue(NO_API_SELECTED);
            }

            Notification notification = new Notification(String.format(S_CALL_PERFORMED, currentDigiStorageAPI), TRAY_NOTIFICATION);
            notification.setPosition(Position.TOP_RIGHT);
            notification.show(this.getPage());

        }
    }

    private void listUsers(String filterText) {
        if (StringUtils.isEmpty(filterText)) {
            grid.setItems(repo.findAll());
        } else {
            grid.setItems(repo.findByNameContainsIgnoreCase(filterText));
        }
    }


    private void videoPlayer(Layout layout, String pathMp4, String pathOgv) {

        Video player = new Video();

        final Resource mp4Resource = new ExternalResource(pathMp4);
        final Resource ogvResource = new ExternalResource(pathOgv);

        player.setSources(mp4Resource, ogvResource);
        player.setSizeFull();

        player.setHtmlContentAllowed(true);
        player.setAltText("Can't play media");

        player.setAutoplay(true);

        layout.removeAllComponents();
        layout.setWidth(String.valueOf(mediaLink.getWidth()));
        layout.addComponent(player);
    }

    private void audioPlayer(Layout layout, String path) {

        Audio player = new Audio();
        final Resource audioResource = new ExternalResource(path);
        player.setSource(audioResource);
        player.setHtmlContentAllowed(true);
        player.setAltText("Can't play media");

        player.setAutoplay(true);

        layout.removeAllComponents();
        layout.addComponent(player);
    }

    private void imageLoader(Layout layout, String path) {
        Image imageLoader = new Image();

        final ExternalResource externalResource = new ExternalResource(path);
        imageLoader.setSource(externalResource);

        //imageLoader.setSource(new ThemeResource(path));

        layout.removeAllComponents();
        layout.addComponent(imageLoader);
    }
}