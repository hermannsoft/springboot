package com.boot.ui.controllers;

import com.boot.model.User;
import com.boot.repository.UserRepository;
import com.vaadin.data.Binder;
import com.vaadin.data.Validator;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.event.ShortcutAction;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * A simple example to introduce building forms. As your real application is probably much
 * more complicated than this example, you could re-use this form in multiple places. This
 * example component is only used in VaadinUI.
 * <p>
 * In a real world application you'll most likely using a common super class for all your
 * forms - less code, better UX. See e.g. AbstractForm in Viritin
 * (https://vaadin.com/addon/viritin).
 */
@SpringComponent
@UIScope
public class UserEditor extends VerticalLayout {

    private final UserRepository repository;

    /* Fields to edit properties in User entity */
    private final TextField name = new TextField("First & Last name");
    private final TextField email = new TextField("Email");
    private final TextField age = new TextField("Age");
    private final TextField token = new TextField("Token");

    /**
     * The currently edited user
     */
    private User user;

    /* Action buttons */
    private Button save = new Button("Save", VaadinIcons.SAFE);
    private Button cancel = new Button("Cancel", VaadinIcons.RECYCLE);
    private Button delete = new Button("Delete", VaadinIcons.TRASH);

    private Binder<User> binder = new Binder<>(User.class);

    @Autowired
    public UserEditor(UserRepository repository) {
        this.repository = repository;

        Validator<Integer> ageValidator = new IntegerRangeValidator(
                "The value must be integer between 0-120 (was {0})",
                0, 120);

        age.addValueChangeListener(e -> validateAge(e.getValue()));

        CssLayout actions = new CssLayout(save, delete);
        addComponents(name, email, age, token, actions);
        token.setEnabled(false);

        binder.forField(this.age)
                .withNullRepresentation("")
                .withConverter(new StringToIntegerConverter(0, "integers only"))
                .bind(User::getAge, User::setAge);

        binder.forField(this.email)
                .withValidator(new EmailValidator("This field must be a RFC 822 standard email addresses"))
                .bind(User::getEmail, User::setEmail);

        // bind using naming convention
        binder.bindInstanceFields(this);
        binder.setBean(user);

        // Configure and style components
        setSpacing(true);
        actions.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
        save.setStyleName(ValoTheme.BUTTON_PRIMARY);
        save.setClickShortcut(ShortcutAction.KeyCode.ENTER);

        // wire action buttons to save, delete and reset
        save.addClickListener(e -> repository.save(user));
        delete.addClickListener(e -> repository.delete(user));
        cancel.addClickListener(e -> editUser(user));
        setVisible(false);
    }

    public final void editUser(User user) {
        if (user == null) {
            setVisible(false);
            return;
        }

        final boolean persisted = user.getId() != null;

        if (persisted) {
            // Find fresh entity for editing
            this.user = repository.findOne(user.getId());
        } else {
            this.user = user;
        }
        cancel.setVisible(persisted);
        delete.setVisible(persisted);

        // Bind user properties to similarly named fields
        // Could also use annotation or "manual binding" or programmatically
        // moving values from fields to entities before saving
        binder.setBean(this.user);

        setVisible(true);

        // A hack to ensure the whole form is visible
        save.focus();

        // Select all text in firstName field automatically
        name.selectAll();
    }

    public void setChangeHandler(ChangeHandler h) {
        // ChangeHandler is notified when either save or delete
        // is clicked
        save.addClickListener(e -> h.onChange());
        delete.addClickListener(e -> h.onChange());
    }

    private void validateAge(String value) {
        boolean isValidNumber = value.matches("\\d{2}");

        if (!isValidNumber) {
            Notification notification = new Notification("Please enter a valid number", Notification.Type.WARNING_MESSAGE);
            notification.setPosition(Position.BOTTOM_LEFT);
            notification.show(this.getUI().getPage());
        }

        save.setEnabled(isValidNumber);
    }

    public interface ChangeHandler {

        void onChange();
    }

}