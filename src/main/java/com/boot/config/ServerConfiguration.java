package com.boot.config;

import com.boot.DasBootApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.EmbeddedServletContainerInitializedEvent;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;

import javax.crypto.Cipher;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.NoSuchAlgorithmException;

@Configuration
public class ServerConfiguration implements ApplicationListener<EmbeddedServletContainerInitializedEvent> {

    private static int port;
    private static String hostname;
    private static String schema;

    @Autowired
    Environment environment;

    @Override
    public void onApplicationEvent(EmbeddedServletContainerInitializedEvent embeddedServletContainerInitializedEvent) {
        port = embeddedServletContainerInitializedEvent.getEmbeddedServletContainer().getPort();

        try {
            int maxKeyLength = Cipher.getMaxAllowedKeyLength("AES");
            InetAddress inetAddress = NetworkInterface.getNetworkInterfaces().nextElement().getInetAddresses().nextElement();
            hostname = inetAddress.getHostName();
            schema = inetAddress.toString();
            System.out.printf("SERVER: IP[%s], DomainName[%s], toString[%s]", inetAddress.getHostAddress(), inetAddress.getCanonicalHostName(), environment);
            System.out.printf("\nAES maxKeyLength[%d]", maxKeyLength);
            System.out.printf("\nPORT[%d] ", port);

            System.out.println();


        } catch (SocketException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @EventListener({ApplicationReadyEvent.class})
    void applicationReadyEvent() {
        DasBootApplication.Browse("http://" + hostname + ":" + port + "?debug");
    }

    public static int getPort() {
        return port;
    }

    public static String getHostname() {
        return hostname;
    }
}
