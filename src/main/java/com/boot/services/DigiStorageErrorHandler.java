package com.boot.services;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;

import java.io.IOException;

public class DigiStorageErrorHandler extends DefaultResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {

        return clientHttpResponse.getRawStatusCode() >= HttpStatus.BAD_REQUEST.value();
    }

    @Override
    public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {

        System.out.println("THREAD handleError: " + clientHttpResponse.getStatusText());


    }
}
