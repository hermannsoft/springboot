package com.boot.services;

import com.boot.model.storage.rds.json.DigiStorageAPI;
import com.boot.model.storage.rds.json.UserBookmarks;
import com.boot.model.storage.rds.json.UserConnections;
import com.boot.model.storage.rds.json.model.Bookmark;
import com.boot.model.storage.rds.json.model.User;
import com.boot.model.storage.rds.json.model.devices.Devices;
import com.boot.model.storage.rds.json.model.files.Blob;
import com.boot.model.storage.rds.json.model.files.Files;
import com.boot.model.storage.rds.json.model.files.Type;
import com.boot.model.storage.rds.json.model.mounts.Mounts;
import com.boot.utils.DasBoot;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Base64;

/**
 * DigiStorage API - http://digistorage.rcs-rds.ro/api/API+DigiStorage
 *
 * @ makowey
 */
public class DigiStorage {

    public static final String MOUNT_ID = "9ba0b9ef-c71c-43ca-a854-5bbd613f729e";
    private String accessToken = "7b6b6de4-3d96-4c0e-b145-197ab8ec5a6e";

    public DigiStorage() {

    }

    public DigiStorage(String accessToken) {
        this.accessToken = accessToken;
    }

    public static void main(String[] args) {
        DigiStorage digiStorage = new DigiStorage();
        //digiStorage.login("makowey@gmail.com", "bd5v edif jb8q swxm");

        System.out.println(digiStorage.getAccessToken());
        System.out.println(digiStorage.getBookmarksFromAPI().toString());

        System.out.println(digiStorage.getMountsFromAPI().toString());
        System.out.println(digiStorage.getDevicesFromAPI().toString());
        Files files = digiStorage.getFilesFromAPI(Type.LIST, MOUNT_ID, "");

        Blob blob = digiStorage.getBlob(MOUNT_ID, files.getFiles().get(0).getName());
        System.out.println(blob.getHowlFile());
    }

    public void login(String email, String password) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new DigiStorageErrorHandler());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        String credentials = String.format("{  \"password\": \"%s\",  \"email\": \"%s\"}", password, email);
        HttpEntity<String> httpEntity = new HttpEntity<>(credentials, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                DigiStorageAPI.API_TOKEN.getAPI(),
                HttpMethod.POST,
                httpEntity,
                String.class);

        accessToken = DasBoot.getValueFromJSON(response.getBody(), "token");
    }

    private DigiRestTemplate getAuthorizedRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new DigiStorageErrorHandler());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", "Token " + accessToken);

        HttpEntity<String> httpEntity = new HttpEntity<>("parameters", headers);

        return new DigiRestTemplate(restTemplate, httpEntity);
    }

    public UserConnections getConnectionsFromAPI() {
        RestTemplate restTemplate = getAuthorizedRestTemplate().restTemplate;

        ResponseEntity<UserConnections> response = restTemplate.exchange(
                DigiStorageAPI.API_CONNECTIONS.getAPI(),
                HttpMethod.GET,
                getAuthorizedRestTemplate().httpEntity,
                UserConnections.class);

        if (response.getStatusCode() != HttpStatus.OK) {
            UserConnections userConnections = new UserConnections(response.toString());
            userConnections.setUsers(new User[0]);

            return userConnections;
        }

        return response.getBody();
    }

    public UserBookmarks getBookmarksFromAPI() {
        RestTemplate restTemplate = getAuthorizedRestTemplate().restTemplate;

        ResponseEntity<UserBookmarks> response = restTemplate.exchange(
                DigiStorageAPI.API_BOOKMARKS.getAPI(),
                HttpMethod.GET,
                getAuthorizedRestTemplate().httpEntity,
                UserBookmarks.class);

        if (response.getStatusCode() != HttpStatus.OK) {
            UserBookmarks userBookmarks = new UserBookmarks(response.toString());
            userBookmarks.setBookmarks(new Bookmark[0]);

            return userBookmarks;
        }

        return response.getBody();
    }

    public Mounts getMountsFromAPI() {
        RestTemplate restTemplate = getAuthorizedRestTemplate().restTemplate;

        ResponseEntity<Mounts> response = restTemplate.exchange(
                DigiStorageAPI.API_MOUNTS.getAPI(),
                HttpMethod.GET,
                getAuthorizedRestTemplate().httpEntity,
                Mounts.class);

        if (response.getStatusCode() != HttpStatus.OK) {
            return new Mounts(response.toString());
        }

        return response.getBody();
    }

    public Devices getDevicesFromAPI() {
        RestTemplate restTemplate = getAuthorizedRestTemplate().restTemplate;

        ResponseEntity<Devices> response = restTemplate.exchange(
                DigiStorageAPI.API_DEVICES.getAPI(),
                HttpMethod.GET,
                getAuthorizedRestTemplate().httpEntity,
                Devices.class);

        if (response.getStatusCode() != HttpStatus.OK) {
            return new Devices(response.toString());
        }

        return response.getBody();
    }

    public Files getFilesFromAPI(Type type, String mountId, String path) {
        RestTemplate restTemplate = getAuthorizedRestTemplate().restTemplate;

        if (path == null) path = "/";
        ResponseEntity<Files> response = restTemplate.exchange(
                DigiStorageAPI.API_FILES.getAPIFiles(type, mountId, path),
                HttpMethod.GET,
                getAuthorizedRestTemplate().httpEntity,
                Files.class);

        if (response.getStatusCode() != HttpStatus.OK) {
            return new Files(response.toString());
        }

        return response.getBody();
    }

    public Blob getBlob(String mountId, String path) {
        RestTemplate restTemplate = getAuthorizedRestTemplate().restTemplate;
        System.out.println("----------- " + path);
        ResponseEntity<Blob> response = restTemplate.exchange(
                DigiStorageAPI.API_FILES.getAPIFiles(Type.DOWNLOAD, mountId, path),
                HttpMethod.GET,
                getAuthorizedRestTemplate().httpEntity,
                Blob.class);

        if (response.getStatusCode() != HttpStatus.OK) {
            return new Blob(response.toString());
        }

        return response.getBody();
    }

    private HttpHeaders createHttpHeaders(String email, String password) {
        String notEncoded = email + ":" + password;
        String encodedAuth = Base64.getEncoder().encodeToString(notEncoded.getBytes());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", "Basic " + encodedAuth);
        return headers;
    }

    public String getAccessToken() {
        return accessToken;
    }

    private class DigiRestTemplate {

        private RestTemplate restTemplate;
        private HttpEntity<String> httpEntity;

        DigiRestTemplate(RestTemplate restTemplate, HttpEntity<String> httpEntity) {
            this.restTemplate = restTemplate;
            this.httpEntity = httpEntity;
        }

    }
}
