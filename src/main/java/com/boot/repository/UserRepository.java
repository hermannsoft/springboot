package com.boot.repository;

import com.boot.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findByNameStartsWithIgnoreCase(String filterText);

    List<User> findByNameContainsIgnoreCase(String filterText);

}
