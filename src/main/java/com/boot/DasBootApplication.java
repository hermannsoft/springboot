package com.boot;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

/**
 * DasBootApplication!
 */
@SpringBootApplication
//@EnableConfigServer
public class DasBootApplication {

    private static String[] browsers = {"epiphany", "firefox", "mozilla", "konqueror",
            "netscape", "opera", "links", "lynx"};

    public static void main(String[] args) {
        SpringApplication.run(DasBootApplication.class, args);
    }

    public static void Browse(String url) {
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            Desktop desktop = Desktop.getDesktop();
            System.out.printf("Application started ... launching browser now on Desktop, url: %s!\n", url);

            try {
                desktop.browse(new URI(url));
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
            }
        } else {
            Runtime runtime = Runtime.getRuntime();
            String os = System.getProperty("os.name").toLowerCase();

            if (os.contains("win")) {
                try {
                    runtime.exec("rundll32 url.dll,FileProtocolHandler " + url);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (os.contains("mac")) {
                try {
                    runtime.exec("open " + url);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (os.contains("nux") || os.contains("nix")) {

                StringBuilder cmd = new StringBuilder();
                for (int i = 0; i < browsers.length; i++)
                    if (i == 0)
                        cmd.append(String.format("%s \"%s\"", browsers[i], url));
                    else
                        cmd.append(String.format(" || %s \"%s\"", browsers[i], url));
                // If the first didn't work, try the next browser and so on

                try {
                    runtime.exec(new String[]{"sh", "-c", cmd.toString()});
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else
                System.out.printf("Operating system %s not supported for auto start\n", os);
        }
    }

    //@Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {

            System.out.println("Let's inspect the beans provided by Spring Boot:");

            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for (String beanName : beanNames) {
                System.out.println(beanName);
            }

        };
    }


}
