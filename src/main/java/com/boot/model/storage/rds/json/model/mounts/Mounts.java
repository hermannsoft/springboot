package com.boot.model.storage.rds.json.model.mounts;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "mounts"
})
public class Mounts {

    @JsonProperty("mounts")
    private List<Mount> mounts = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    private String stateObject = "";

    public Mounts(String s) {
        stateObject = s;
    }

    public Mounts() {
    }

    @JsonProperty("mounts")
    public List<Mount> getMounts() {
        return mounts;
    }

    @JsonProperty("mounts")
    public void setMounts(List<Mount> mounts) {
        this.mounts = mounts;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public String getStateObject() {
        return stateObject;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("user/mounts\n\n");

        int counter = 0;
        for (Mount mount : mounts) {

            stringBuilder.append(
                    String.format("\tmount[%d]:\n" +
                                    "\t\tid: %s\n" +
                                    "\t\tname: %s\n" +
                                    "\t\tdeviceId: %s\n",
                            ++counter,
                            mount.getId(), mount.getName(), mount.getDeviceId()));

            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }
}
