package com.boot.model.storage.rds.json.model.files;

import com.google.gson.Gson;

public class HowlFile {

    private String title;
    private String file;
    private String howl;

    public HowlFile(String title, String file, String howl) {
        this.title = title;
        this.file = file;
        this.howl = howl;
    }

    public String getTitle() {
        return title;
    }

    public String getFile() {
        return file;
    }

    public String getHowl() {
        return howl;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
