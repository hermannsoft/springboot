
package com.boot.model.storage.rds.json.model.devices;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "apiKey",
    "name",
    "status",
    "spaceTotal",
    "spaceUsed",
    "spaceFree",
    "version",
    "provider",
    "canEdit",
    "canRemove",
    "rootMountId",
    "searchEnabled"
})
public class Device {

    @JsonProperty("id")
    private String id;
    @JsonProperty("apiKey")
    private String apiKey;
    @JsonProperty("name")
    private String name;
    @JsonProperty("status")
    private String status;
    @JsonProperty("spaceTotal")
    private Integer spaceTotal;
    @JsonProperty("spaceUsed")
    private Integer spaceUsed;
    @JsonProperty("spaceFree")
    private Integer spaceFree;
    @JsonProperty("version")
    private Integer version;
    @JsonProperty("provider")
    private Provider provider;
    @JsonProperty("canEdit")
    private Boolean canEdit;
    @JsonProperty("canRemove")
    private Boolean canRemove;
    @JsonProperty("rootMountId")
    private String rootMountId;
    @JsonProperty("searchEnabled")
    private Boolean searchEnabled;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("apiKey")
    public String getApiKey() {
        return apiKey;
    }

    @JsonProperty("apiKey")
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("spaceTotal")
    public Integer getSpaceTotal() {
        return spaceTotal;
    }

    @JsonProperty("spaceTotal")
    public void setSpaceTotal(Integer spaceTotal) {
        this.spaceTotal = spaceTotal;
    }

    @JsonProperty("spaceUsed")
    public Integer getSpaceUsed() {
        return spaceUsed;
    }

    @JsonProperty("spaceUsed")
    public void setSpaceUsed(Integer spaceUsed) {
        this.spaceUsed = spaceUsed;
    }

    @JsonProperty("spaceFree")
    public Integer getSpaceFree() {
        return spaceFree;
    }

    @JsonProperty("spaceFree")
    public void setSpaceFree(Integer spaceFree) {
        this.spaceFree = spaceFree;
    }

    @JsonProperty("version")
    public Integer getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(Integer version) {
        this.version = version;
    }

    @JsonProperty("provider")
    public Provider getProvider() {
        return provider;
    }

    @JsonProperty("provider")
    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    @JsonProperty("canEdit")
    public Boolean getCanEdit() {
        return canEdit;
    }

    @JsonProperty("canEdit")
    public void setCanEdit(Boolean canEdit) {
        this.canEdit = canEdit;
    }

    @JsonProperty("canRemove")
    public Boolean getCanRemove() {
        return canRemove;
    }

    @JsonProperty("canRemove")
    public void setCanRemove(Boolean canRemove) {
        this.canRemove = canRemove;
    }

    @JsonProperty("rootMountId")
    public String getRootMountId() {
        return rootMountId;
    }

    @JsonProperty("rootMountId")
    public void setRootMountId(String rootMountId) {
        this.rootMountId = rootMountId;
    }

    @JsonProperty("searchEnabled")
    public Boolean getSearchEnabled() {
        return searchEnabled;
    }

    @JsonProperty("searchEnabled")
    public void setSearchEnabled(Boolean searchEnabled) {
        this.searchEnabled = searchEnabled;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
