package com.boot.model.storage.rds.json;

import com.boot.model.storage.rds.json.model.Bookmark;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserBookmarks {

    private Bookmark[] bookmarks;

    private String stateObject = "";

    public UserBookmarks() {}

    public UserBookmarks(String stateObject) {
        this.stateObject = stateObject;
    }

    public Bookmark[] getBookmarks() {
        return bookmarks;
    }

    public void setBookmarks(Bookmark[] bookmarks) {
        this.bookmarks = bookmarks;
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder("user/bookmarks\n\n");

        int counter = 0;
        for (Bookmark bookmark : bookmarks) {

            stringBuilder.append(
                    String.format("\tbookmark[%d]:\n" +
                                    "\t\tmount: %s\n" +
                                    "\t\tname: %s\n" +
                                    "\t\tpath: %s\n",
                            ++counter,
                            bookmark.getMountId(), bookmark.getName(), bookmark.getPath()));

            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }

    public String getStateObject() {
        return stateObject;
    }
}
