
package com.boot.model.storage.rds.json.model.mounts;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "READ",
    "OWNER",
    "MOUNT",
    "CREATE_RECEIVER",
    "COMMENT",
    "WRITE",
    "CREATE_LINK"
})
public class Permissions__ {

    @JsonProperty("READ")
    private Boolean rEAD;
    @JsonProperty("OWNER")
    private Boolean oWNER;
    @JsonProperty("MOUNT")
    private Boolean mOUNT;
    @JsonProperty("CREATE_RECEIVER")
    private Boolean cREATERECEIVER;
    @JsonProperty("COMMENT")
    private Boolean cOMMENT;
    @JsonProperty("WRITE")
    private Boolean wRITE;
    @JsonProperty("CREATE_LINK")
    private Boolean cREATELINK;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("READ")
    public Boolean getREAD() {
        return rEAD;
    }

    @JsonProperty("READ")
    public void setREAD(Boolean rEAD) {
        this.rEAD = rEAD;
    }

    @JsonProperty("OWNER")
    public Boolean getOWNER() {
        return oWNER;
    }

    @JsonProperty("OWNER")
    public void setOWNER(Boolean oWNER) {
        this.oWNER = oWNER;
    }

    @JsonProperty("MOUNT")
    public Boolean getMOUNT() {
        return mOUNT;
    }

    @JsonProperty("MOUNT")
    public void setMOUNT(Boolean mOUNT) {
        this.mOUNT = mOUNT;
    }

    @JsonProperty("CREATE_RECEIVER")
    public Boolean getCREATERECEIVER() {
        return cREATERECEIVER;
    }

    @JsonProperty("CREATE_RECEIVER")
    public void setCREATERECEIVER(Boolean cREATERECEIVER) {
        this.cREATERECEIVER = cREATERECEIVER;
    }

    @JsonProperty("COMMENT")
    public Boolean getCOMMENT() {
        return cOMMENT;
    }

    @JsonProperty("COMMENT")
    public void setCOMMENT(Boolean cOMMENT) {
        this.cOMMENT = cOMMENT;
    }

    @JsonProperty("WRITE")
    public Boolean getWRITE() {
        return wRITE;
    }

    @JsonProperty("WRITE")
    public void setWRITE(Boolean wRITE) {
        this.wRITE = wRITE;
    }

    @JsonProperty("CREATE_LINK")
    public Boolean getCREATELINK() {
        return cREATELINK;
    }

    @JsonProperty("CREATE_LINK")
    public void setCREATELINK(Boolean cREATELINK) {
        this.cREATELINK = cREATELINK;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
