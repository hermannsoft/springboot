package com.boot.model.storage.rds.json.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Embeddable;

@JsonIgnoreProperties(ignoreUnknown = true)
@Embeddable
public class Bookmark {

    private String mountId;
    private String name;
    private String path;

    public Bookmark() {
    }

    public String getMountId() {
        return mountId;
    }

    public void setMountId(String mountId) {
        this.mountId = mountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}