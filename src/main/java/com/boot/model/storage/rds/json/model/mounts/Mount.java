
package com.boot.model.storage.rds.json.model.mounts;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.boot.model.storage.rds.json.model.User;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name",
    "type",
    "origin",
    "online",
    "owner",
    "users",
    "groups",
    "isShared",
    "permissions",
    "spaceTotal",
    "spaceUsed",
    "version",
    "isPrimary",
    "canWrite",
    "canUpload",
    "overQuota",
    "almostOverQuota",
    "userAdded",
    "capabilities",
    "deviceId",
    "isDir",
    "root"
})
public class Mount {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("type")
    private String type;
    @JsonProperty("origin")
    private String origin;
    @JsonProperty("online")
    private Boolean online;
    @JsonProperty("owner")
    private Owner owner;
    @JsonProperty("users")
    private List<User> users = null;
    @JsonProperty("groups")
    private List<Object> groups = null;
    @JsonProperty("isShared")
    private Boolean isShared;
    @JsonProperty("permissions")
    private Permissions__ permissions;
    @JsonProperty("spaceTotal")
    private Integer spaceTotal;
    @JsonProperty("spaceUsed")
    private Integer spaceUsed;
    @JsonProperty("version")
    private Integer version;
    @JsonProperty("isPrimary")
    private Boolean isPrimary;
    @JsonProperty("canWrite")
    private Boolean canWrite;
    @JsonProperty("canUpload")
    private Boolean canUpload;
    @JsonProperty("overQuota")
    private Boolean overQuota;
    @JsonProperty("almostOverQuota")
    private Boolean almostOverQuota;
    @JsonProperty("userAdded")
    private Long userAdded;
    @JsonProperty("capabilities")
    private Capabilities capabilities;
    @JsonProperty("deviceId")
    private String deviceId;
    @JsonProperty("isDir")
    private Boolean isDir;
    @JsonProperty("root")
    private Root root;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("origin")
    public String getOrigin() {
        return origin;
    }

    @JsonProperty("origin")
    public void setOrigin(String origin) {
        this.origin = origin;
    }

    @JsonProperty("online")
    public Boolean getOnline() {
        return online;
    }

    @JsonProperty("online")
    public void setOnline(Boolean online) {
        this.online = online;
    }

    @JsonProperty("owner")
    public Owner getOwner() {
        return owner;
    }

    @JsonProperty("owner")
    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    @JsonProperty("users")
    public List<User> getUsers() {
        return users;
    }

    @JsonProperty("users")
    public void setUsers(List<User> users) {
        this.users = users;
    }

    @JsonProperty("groups")
    public List<Object> getGroups() {
        return groups;
    }

    @JsonProperty("groups")
    public void setGroups(List<Object> groups) {
        this.groups = groups;
    }

    @JsonProperty("isShared")
    public Boolean getIsShared() {
        return isShared;
    }

    @JsonProperty("isShared")
    public void setIsShared(Boolean isShared) {
        this.isShared = isShared;
    }

    @JsonProperty("permissions")
    public Permissions__ getPermissions() {
        return permissions;
    }

    @JsonProperty("permissions")
    public void setPermissions(Permissions__ permissions) {
        this.permissions = permissions;
    }

    @JsonProperty("spaceTotal")
    public Integer getSpaceTotal() {
        return spaceTotal;
    }

    @JsonProperty("spaceTotal")
    public void setSpaceTotal(Integer spaceTotal) {
        this.spaceTotal = spaceTotal;
    }

    @JsonProperty("spaceUsed")
    public Integer getSpaceUsed() {
        return spaceUsed;
    }

    @JsonProperty("spaceUsed")
    public void setSpaceUsed(Integer spaceUsed) {
        this.spaceUsed = spaceUsed;
    }

    @JsonProperty("version")
    public Integer getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(Integer version) {
        this.version = version;
    }

    @JsonProperty("isPrimary")
    public Boolean getIsPrimary() {
        return isPrimary;
    }

    @JsonProperty("isPrimary")
    public void setIsPrimary(Boolean isPrimary) {
        this.isPrimary = isPrimary;
    }

    @JsonProperty("canWrite")
    public Boolean getCanWrite() {
        return canWrite;
    }

    @JsonProperty("canWrite")
    public void setCanWrite(Boolean canWrite) {
        this.canWrite = canWrite;
    }

    @JsonProperty("canUpload")
    public Boolean getCanUpload() {
        return canUpload;
    }

    @JsonProperty("canUpload")
    public void setCanUpload(Boolean canUpload) {
        this.canUpload = canUpload;
    }

    @JsonProperty("overQuota")
    public Boolean getOverQuota() {
        return overQuota;
    }

    @JsonProperty("overQuota")
    public void setOverQuota(Boolean overQuota) {
        this.overQuota = overQuota;
    }

    @JsonProperty("almostOverQuota")
    public Boolean getAlmostOverQuota() {
        return almostOverQuota;
    }

    @JsonProperty("almostOverQuota")
    public void setAlmostOverQuota(Boolean almostOverQuota) {
        this.almostOverQuota = almostOverQuota;
    }

    @JsonProperty("userAdded")
    public Long getUserAdded() {
        return userAdded;
    }

    @JsonProperty("userAdded")
    public void setUserAdded(Long userAdded) {
        this.userAdded = userAdded;
    }

    @JsonProperty("capabilities")
    public Capabilities getCapabilities() {
        return capabilities;
    }

    @JsonProperty("capabilities")
    public void setCapabilities(Capabilities capabilities) {
        this.capabilities = capabilities;
    }

    @JsonProperty("deviceId")
    public String getDeviceId() {
        return deviceId;
    }

    @JsonProperty("deviceId")
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @JsonProperty("isDir")
    public Boolean getIsDir() {
        return isDir;
    }

    @JsonProperty("isDir")
    public void setIsDir(Boolean isDir) {
        this.isDir = isDir;
    }

    @JsonProperty("root")
    public Root getRoot() {
        return root;
    }

    @JsonProperty("root")
    public void setRoot(Root root) {
        this.root = root;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
