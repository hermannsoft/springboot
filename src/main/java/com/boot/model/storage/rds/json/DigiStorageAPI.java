package com.boot.model.storage.rds.json;

import com.boot.model.storage.rds.json.model.files.Type;

public enum DigiStorageAPI {

    API_TOKEN("/token"),
    API_BOOKMARKS("/api/v2/user/bookmarks"),
    API_CONNECTIONS("/api/v2/user/connections"),
    API_MOUNTS("/api/v2/mounts"),
    API_DEVICES("/api/v2/devices"),
    API_FILES("/api/v2/mounts/{{MOUNT_ID}}/files/{{TYPE}}?path=/{{PATH}}");

    private String API;
    private final String API_SERVER = "https://storage.rcs-rds.ro";

    DigiStorageAPI(String api) {
        this.API = API_SERVER.concat(api);
    }

    private void resetToDefaultAPIFiles() {
        this.API = API_SERVER.concat("/api/v2/mounts/{{MOUNT_ID}}/files/{{TYPE}}?path=/{{PATH}}");
    }
    public String getAPI() {
        return API;
    }

    public String getAPIFiles(Type type, String mountId, String path) {

        resetToDefaultAPIFiles();

        setMountId(mountId);
        setPath(path);
        setType(type);
        return API;
    }

    public String getName() {
        return this.name();
    }

    public void setMountId(String mountId) {
        this.API = this.API.replace("{{MOUNT_ID}}", mountId);
    }

    public void setPath(String path) {
        this.API = this.API.replace("{{PATH}}", path);
    }

    public void setType(Type type) {
        this.API = this.API.replace("{{TYPE}}", type.getName());
    }
}
