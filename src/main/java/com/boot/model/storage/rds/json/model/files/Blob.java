package com.boot.model.storage.rds.json.model.files;

import com.fasterxml.jackson.annotation.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "link"
})
public class Blob {

    public static final String CONTENT_FILES_GET = "/content/files/get/";
    public static final String BASE = "base=";

    @JsonProperty("link")
    private String link;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    private String stateObject = "";

    public Blob(String s) {
        stateObject = s;
    }

    public Blob() {
    }

    @JsonProperty("link")
    public String getLink() {
        return link;
    }

    @JsonProperty("link")
    public void setLink(String link) {
        this.link = link;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public String getStateObject() {
        return stateObject;
    }

    public String getFilename() {
        try {
            URL url = new URL(link);

            return url.getPath().replace(CONTENT_FILES_GET, "");
        } catch (MalformedURLException e) {
            System.err.println("Invalid URL " + link);
        }

        return "ERROR";
    }

    public String getFileHash() {
        try {
            URL url = new URL(link);

            return url.getQuery().replace(BASE, "");
        } catch (MalformedURLException e) {
            System.err.println("Invalid URL " + link);
        }

        return "ERROR";
    }

    public HowlFile getHowlFile() {
        return new HowlFile(getFilename(), getFileHash(), null);
    }

    @Override
    public String toString() {
        return "Blob{" +
                "link='" + link + '\'' +
                ", additionalProperties=" + additionalProperties +
                ", stateObject='" + stateObject + '\'' +
                '}';
    }
}