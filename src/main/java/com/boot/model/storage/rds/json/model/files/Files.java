
package com.boot.model.storage.rds.json.model.files;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.boot.model.storage.rds.json.model.mounts.Mount;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "files"
})
public class Files {

    @JsonProperty("files")
    private List<File> files = null;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    private String stateObject = "";

    public Files(String s) {
        stateObject = s;
    }

    public Files() {
    }

    @JsonProperty("files")
    public List<File> getFiles() {
        return files;
    }

    @JsonProperty("files")
    public void setFiles(List<File> files) {
        this.files = files;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public String getStateObject() {
        return stateObject;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("mounts/{id}/files\n\n");

        int counter = 0;
        for (File file : files) {
            stringBuilder.append(
                    String.format("\tfile[%d]:\n" +
                                    "\t\tname: %s\n" +
                                    "\t\tcontentType: %s\n" +
                                    "\t\tsize: %s\n",
                            ++counter,
                            file.getName(), file.getContentType(), file.getSize()));

            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }
}
