package com.boot.model.storage.rds.json.model.files;

public enum Type {
    LIST, TREE, DOWNLOAD;

    public String getName() {
        return this.name().toLowerCase();
    }
}
