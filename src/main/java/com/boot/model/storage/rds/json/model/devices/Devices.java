
package com.boot.model.storage.rds.json.model.devices;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.boot.model.storage.rds.json.model.mounts.Mount;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "devices"
})
public class Devices {

    @JsonProperty("devices")
    private List<Device> devices = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    private String stateObject = "";

    public Devices(String s) {
        stateObject = s;
    }

    public Devices() {
    }

    @JsonProperty("devices")
    public List<Device> getDevices() {
        return devices;
    }

    @JsonProperty("devices")
    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public String getStateObject() {
        return stateObject;
    }
    
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("user/devices\n\n");

        int counter = 0;
        for (Device device : devices) {

            stringBuilder.append(
                    String.format("\tdevice[%d]:\n" +
                                    "\t\tid: %s\n" +
                                    "\t\tname: %s\n" +
                                    "\t\tapiKey: %s\n",
                            ++counter,
                            device.getId(), device.getName(), device.getApiKey()));

            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }
}
