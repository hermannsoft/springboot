package com.boot.model.storage.rds.json;

import com.boot.model.storage.rds.json.model.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserConnections {

    private String[] groups;

    private User[] users;

    private String stateObject = "";

    public UserConnections(){}

    public UserConnections(String stateObject) {
        this.stateObject = stateObject;
    }

    public String[] getGroups() {
        return groups;
    }

    public void setGroups(String[] groups) {
        this.groups = groups;
    }

    public User[] getUsers() {
        return users;
    }

    public void setUsers(User[] users) {
        this.users = users;
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder("user/connections\n\n");

        int counter = 0;
        for (User user : users) {

            stringBuilder.append(
                    String.format("\tuser[%d]:\n" +
                                    "\t\temail: %s\n" +
                                    "\t\tid: %s\n" +
                                    "\t\tname: %s\n",
                            ++counter,
                            user.getEmail(), user.getId(), user.getName()));

            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }

    public String getStateObject() {
        return stateObject;
    }
}
