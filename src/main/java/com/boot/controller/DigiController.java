package com.boot.controller;

import com.boot.model.storage.rds.json.model.files.Blob;
import com.boot.model.storage.rds.json.model.files.File;
import com.boot.model.storage.rds.json.model.files.HowlFile;
import com.boot.model.storage.rds.json.model.files.Type;
import com.boot.services.DigiStorage;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("digi/")
public class DigiController {

    private DigiStorage digiStorage = new DigiStorage();

    @RequestMapping(value = "files/mountId:{mountId}:original", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<File> listMusicFiles(@PathVariable String mountId) {
        return digiStorage.getFilesFromAPI(Type.LIST, mountId, "").getFiles().
                parallelStream()
                .filter(file -> file.getName().endsWith(MediaPlayerExtension.MP3.extension))
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "files/mountId:{mountId}:player", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<HowlFile> listHowlFiles(@PathVariable String mountId) {
        return listMusicFiles(mountId)
                .parallelStream()
                .filter(file -> file.getName().endsWith(MediaPlayerExtension.MP3.extension))
                .map(file -> {
                    Blob blob = digiStorage.getBlob(mountId, file.getName());
                    return new HowlFile(blob.getFilename(), blob.getFileHash(), null);
                })
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "files/mountId:{mountId}/file:{filename}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    HowlFile getHowlFile(@PathVariable String mountId, @PathVariable String filename) {
        return digiStorage.getBlob(mountId, filename).getHowlFile();
    }

    private enum MediaPlayerExtension {
        MP3("mp3"), OGG("ogg");

        private String extension;

        MediaPlayerExtension(String fileExtension) {
            this.extension = fileExtension;
        }

        @Override
        public String toString() {
            return extension;
        }
    }
}
