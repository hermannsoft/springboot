package com.boot.controller;

import com.boot.model.User;
import com.boot.repository.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "users", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<User> list() {
        return userRepository.findAll();
    }

    @RequestMapping(value = "users/{id}", method = RequestMethod.GET)
    public @ResponseBody
    User getById(@PathVariable Long id) {
        return userRepository.findOne(id);
    }

    @RequestMapping(value = "users/name:{name}", method = RequestMethod.GET)
    public @ResponseBody
    List<User> getByName(@PathVariable String name) {

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains());

        Example<User> example = Example.of(new User(name), matcher);

        return userRepository.findAll(example);
    }


    @RequestMapping(value = "users", method = RequestMethod.POST)
    public @ResponseBody
    User create(@RequestBody User user) {
        return userRepository.saveAndFlush(user);
    }

    @RequestMapping(value = "users/{id}", method = RequestMethod.PUT)
    public @ResponseBody
    User update(@PathVariable Long id, @RequestBody User user) {
        User existingUser = userRepository.findOne(id);
        BeanUtils.copyProperties(user, existingUser);
        return userRepository.saveAndFlush(existingUser);
    }


    @RequestMapping(value = "users/{id}", method = RequestMethod.DELETE)
    public @ResponseBody
    String delete(@PathVariable Long id) {
        User existingUser = userRepository.findOne(id);
        userRepository.delete(id);

        return existingUser.getName().concat(" DELETED!");
    }
}
