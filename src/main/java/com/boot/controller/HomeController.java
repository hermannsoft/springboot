package com.boot.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {


    @GetMapping(path = "/home")
    public String home() {
        return "Das Boot, reporting for duty!";
    }

    @GetMapping(path = "/contact")
    public String contact() {
        return "@claudiu";
    }
}
