package com.boot.utils.observable;

public class StrangeClass {

    public static void main(String... args) {
        System.out.printf("%b", "tree".equals("tree"));

        printValues(10);
        printValues(9, 11);
        printValues(7, 11, 12, 15);
    }

    private static void printValues(int value, int... moreValues) {
        System.out.print("First value: " + value + " // ");

        for (int otherValue : moreValues) {
            System.out.printf(otherValue + ", ");
        }

        System.out.println("************************");
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
