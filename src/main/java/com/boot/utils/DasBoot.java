package com.boot.utils;

import com.boot.model.User;

import java.io.File;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.joining;

public class DasBoot {

    private static List<Integer> list = Arrays.asList(1, 2, 3, 5, 4, 6, 7, 8, 9, 10);

    /**
     * @param json
     * @param key
     * @return
     */
    public static String getValueFromJSON(String json, String key) {
        if (!json.contains(key)) {
            throw new IllegalArgumentException("Key '" + key + "' not found in Json source!");
        }
        return json.split("\"" + key + "\":")[1].split("(,\"|\\}$)")[0].replaceAll("(^\"|\"$)", "");
    }

    public static void main(String[] args) {
        System.out.println("Das Boot!");

        Vector<Integer> vector = new Vector<>(list);

        vector.removeIf(integer -> integer < 3);
        vector.forEach(System.out::println);

        System.out.printf("Total values = %d\n", totalValues(list, e -> true));
        System.out.printf("Total values even = %d\n", totalValues(list, e -> e % 2 == 0));
        System.out.printf("Total values odd = %d\n", totalValues(list, e -> e % 2 != 0));

        list.stream()
                .filter(Computation.greaterThan(5))
                .filter(Computation.isEven())
                .map(Computation.doubleIt())
                .findFirst()
                .ifPresent(value -> System.out.printf("First double even number in the list greater than 4 is %d\n", value));

        Timeit.code(() -> {
            Stream.iterate(1, e -> e + 1)
                    .filter(Computation::isPrime)
                    .map(Math::abs)
                    .limit(100)
                    .forEach((value) -> System.out.printf("%d, ", value));
        });

        File file = new File(".");

        File[] children = file.listFiles();
        if (children != null) {

            System.out.println(
                    Stream.of(children)
                            .map(File::getName)
                            .map(String::toUpperCase)
                            .collect(joining(", "))
            );
        }

        List<User> users = Arrays.asList(
                new User("John", 25),
                new User("Adam", 969),
                new User("Superman", 1_000)
        );

        users.forEach(System.out::println);

        Comparator<User> comparatorByName = comparing(User::getName);
        users.stream()
                .sorted(comparatorByName)
                .forEach(System.out::println);

        Comparator<User> comparatorByAge = comparing(User::getAge)
                .thenComparing(User::getName)
                .reversed();

        users.stream()
                .sorted(comparatorByAge)
                .forEach(System.out::println);

        System.out.println(
                users.stream()
                        .sorted(comparatorByName)
                        .map(User::getName)
                        .collect(joining(", "))
        );

        System.out.println("some bechmarks....");
        benchmark();

        assert "test".equals("test");
    }

    public static int totalValues(List<Integer> numbers, Predicate<Integer> selector) {
        return numbers
                .stream()
                .filter(selector)
                .reduce(0, (c, e) -> c + e);
    }

    public static void benchmark() {

        int[] ints = new int[500_000_000];
        Random random = new Random(ints.length);
        for (int i = 0; i < ints.length; i++) {
            ints[i] = random.nextInt();
        }

        int[] a = ints;
        int e = ints.length;

        Timeit.code(() -> {
            int m = Integer.MIN_VALUE;
            for (int i = 0; i < e; i++)
                if (a[i] > m) m = a[i];

            System.out.printf(" max: %d", m);
        });

        Timeit.code(() -> {
            int m = Arrays.stream(ints)
                    .reduce(Integer.MIN_VALUE, Math::max);

            System.out.printf(" max: %d", m);
        });

        Timeit.code(() -> {
            int m = Arrays.stream(ints).parallel()
                    .reduce(Integer.MIN_VALUE, Math::max);

            System.out.printf(" max: %d", m);
        });
    }

}
