package com.boot.utils;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.IntStream;

public final class Computation {

    private Computation() {
    }

    public static Predicate<Integer> greaterThan(Integer value) {
        return integer -> integer > value;
    }

    public static Predicate<Integer> isEven() {
        return integer -> integer % 2 == 0;
    }

    public static Function<Integer, Integer> doubleIt() {
        return integer -> integer * 2;
    }

    public static boolean isPrime(Integer number) {
        return number > 1 &&
                IntStream.range(2, number)
                        .noneMatch(i -> number % i == 0);
    }
}
