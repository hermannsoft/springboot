CREATE TABLE USER(
  ID INT AUTO_INCREMENT,
  NAME VARCHAR(255),
  AGE INT,
  EMAIL VARCHAR(255) NOT NULL UNIQUE,
  PASSWORD VARCHAR(255),
  TOKEN VARCHAR(255) NOT NULL
);

INSERT INTO USER(ID, NAME, AGE, EMAIL, PASSWORD, TOKEN)
  VALUES
    (DEFAULT , 'John Doe', 34, 'user1@email.com', '**********', 'abcd-dec'),
    (DEFAULT , 'John Smith', 45, 'user2@email.com', '**********', 'abcg-dec'),
    (DEFAULT , 'Bill Wayne', 66, 'user3@email.com', '**********', 'abcf-dec');
